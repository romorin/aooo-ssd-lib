# @todo catch2 checking
# @todo better way to list sources?
# @todo dependencies hard coded

cmake_minimum_required(VERSION 3.1...3.14)

if(${CMAKE_VERSION} VERSION_LESS 3.12)
    cmake_policy(VERSION ${CMAKE_MAJOR_VERSION}.${CMAKE_MINOR_VERSION})
endif()

project(Boo-ssd VERSION 1.0
	DESCRIPTION "Boo classes to handle seven segments displays"
	LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 11)

set(TEST_SOURCES
	test/main.cpp
	test/charSelectionCenterTests.cpp
	test/charSelectionLeftTests.cpp
	test/charSelectionRightTests.cpp
	test/charSelectionScrollTests.cpp
	test/charSelectorTests.cpp
	test/ssdContentTests.cpp
	../arduino-utils/src/shiftedCounter.cpp
	../arduino-utils/src/timerImpl.cpp
	src/charSelectionCenter.cpp
	src/charSelectionLeft.cpp
	src/charSelectionRight.cpp
	src/charSelectionScroll.cpp
	src/charSelector.cpp
	src/segmentsDict.cpp)

add_executable(tests ${TEST_SOURCES})
target_include_directories(tests PUBLIC include)
target_include_directories(tests PUBLIC ../common)
target_include_directories(tests PUBLIC ../arduino-utils/include)
