#include "catch2/catch.hpp"

#include "CharSelectionRight.h"
#include "stubs/SsdContentStub.h"

#include <string>

SCENARIO( "CharSelectionRight works" ) {
	GIVEN( "The strategy" ) {
		CharSelectionRight selectionStrategy;
		SsdContentStub content;
		content.segment = 'z';
		content.len = 2;

		WHEN ( "A short text is used" ) {
			THEN ( "the caracters are correctly given" ) {
				REQUIRE( selectionStrategy.getSegments(1, 3, content) == 'z' );
				REQUIRE( content.position == 0 );

				REQUIRE( selectionStrategy.getSegments(2, 3, content) == 'z' );
				REQUIRE( content.position == 1 );
			}

			AND_THEN( "empty chars are given before the content" ) {
				REQUIRE( selectionStrategy.getSegments(0, 3, content) == 'z' );
				REQUIRE( content.value == ' ' );
				REQUIRE( !content.withDot );
			}

			AND_THEN( "empty chars are given out of bounds" ) {
				REQUIRE( selectionStrategy.getSegments(3, 3, content) == 'z' );
				REQUIRE( content.value == ' ' );
				REQUIRE( !content.withDot );
			}
		}
	}
}
