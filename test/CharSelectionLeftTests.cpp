#include "catch2/catch.hpp"

#include "CharSelectionLeft.h"
#include "stubs/SsdContentStub.h"

#include <string>

SCENARIO( "CharSelectionLeft works" ) {
	GIVEN( "The strategy" ) {
		CharSelectionLeft leftSelectionStrategy;
		SsdContentStub content;
		content.segment = 'z';
		content.len = 2;

		WHEN ( "A short text is used" ) {
			THEN ( "the caracters are correctly given" ) {
				REQUIRE( leftSelectionStrategy.getSegments(0, 3, content) == 'z' );
				REQUIRE( content.position == 0 );

				REQUIRE( leftSelectionStrategy.getSegments(1, 3, content) == 'z' );
				REQUIRE( content.position == 1 );
			}

			AND_THEN( "empty chars are given past the content" ) {
				REQUIRE( leftSelectionStrategy.getSegments(2, 3, content) == 'z' );
				REQUIRE( content.value == ' ' );
				REQUIRE( !content.withDot );
			}

			AND_THEN( "empty chars are given out of bounds" ) {
				REQUIRE( leftSelectionStrategy.getSegments(3, 3, content) == 'z' );
				REQUIRE( content.value == ' ' );
				REQUIRE( !content.withDot );
			}
		}
	}
}
