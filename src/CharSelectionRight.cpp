#include "CharSelectionRight.h"

#include <cstring>

CharSelectionRight::CharSelectionRight()
{}

CharSelectionRight::~CharSelectionRight()
{}

char CharSelectionRight::getSegments(
	const size_t position,
	const size_t availableDigits,
	const SsdContent& content) const
{
	size_t pad = availableDigits - content.length();

	if (position < pad || position >= availableDigits)
	{
		return content.getSegments(' ', false);
	}
	else
	{
		return content.getSegments(position-pad);
	}
}
