#include "SegmentsDict.h"

#include <algorithm>
#include <cctype>

class SegmentsDictEntry
{
public:
	SegmentsDictEntry(char key, char segments) :
		_key(key), _segments(segments) {}

	char key() { return _key; }
	char segments() { return _segments; }

private:
	char _key;
	char _segments;
};

bool _mapInitiated = false;
std::array<SegmentsDictEntry, NUM_SEGMENTS_DICT_ENTRIES> _segmentsMap = {{
	{' ', 0b00000000},
	{'-', 0b01000000},
	{'0', 0b00111111},
	{'1', 0b00000110},
	{'2', 0b01011011},
	{'3', 0b01001111},
	{'4', 0b01100110},
	{'5', 0b01101101},
	{'6', 0b01111101},
	{'7', 0b00000111},
	{'8', 0b01111111},
	{'9', 0b01101111},
	{'a', 0b01110111},
	{'b', 0b01111100},
	{'c', 0b00111001},
	{'d', 0b01011110},
	{'e', 0b01111001},
	{'f', 0b01110001},
	{'g', 0b00111101},
	{'h', 0b01110110},
	{'i', 0b00110000},
	{'j', 0b00011110},
	{'k', 0b01111000},
	{'l', 0b00111000},
	{'m', 0b01010101},
	{'n', 0b01010100},
	{'o', 0b01011100},
	{'p', 0b01110011},
	{'q', 0b01100111},
	{'r', 0b01010000},
	{'s', 0b00101101},
	{'t', 0b00110001},
	{'u', 0b00111110},
	{'v', 0b01110010},
	{'w', 0b00011101},
	{'x', 0b01000110},
	{'y', 0b01100110},
	{'z', 0b00011011}
}};

bool compareEntries(SegmentsDictEntry i, SegmentsDictEntry j)
{
	return (i.key() < j.key());
}

void initMap()
{
	if (_mapInitiated == false)
	{
		std::sort(_segmentsMap.begin(), _segmentsMap.end(),
			compareEntries);
	}
}

SegmentsDict::SegmentsDict()
{
	initMap();
}

const unsigned char SegmentsDict::getSegments(
	const char value, const bool withDot) const
{
	SegmentsDictEntry dummy = { (char)tolower(value), '0'};
	auto found = std::lower_bound(_segmentsMap.begin(), _segmentsMap.end(),
		dummy, compareEntries);

	if (found == _segmentsMap.end())
	{
		return 0;
	}
	else
	{
		return found->segments() | (withDot ? (1 << 7) : 0);
	}
}
