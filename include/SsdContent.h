#ifndef SSD_CONTENT_H
#define SSD_CONTENT_H

#include "SegmentsDict.h"

#include "stddef.h"

class SsdContent
{
public:
	virtual ~SsdContent() {}

	virtual void writeString(const char* const entry) = 0;

	virtual size_t length() const = 0;

	virtual unsigned char getSegments(const size_t position) const = 0;

	virtual unsigned char getSegments(const char value, const bool withDot) const = 0;
};

#endif
