#ifndef CHAR_SELECTOR_H
#define CHAR_SELECTOR_H

#include "CharStrategies.h"
#include "CharSelectionStrategy.h"
#include "SsdContent.h"

#include "stddef.h"

class CharSelector
{
public:
	CharSelector(SsdContent& content,
		const CharSelectionStrategy& centerSelectionStrategy,
		const CharSelectionStrategy& leftSelectionStrategy,
		const CharSelectionStrategy& rightSelectionStrategy,
		const CharSelectionStrategy& beginningOverflowStrategy,
		const CharSelectionStrategy& errorOverflowStrategy,
		const CharSelectionStrategy& scrollOverflowStrategy);

	void writeString(const char* const entry);

	void setPositioningStrategy(
		const PositioningStrategy strategy);
	void setOverflowStrategy(
		const OverflowStrategy strategy);

	unsigned char getSegments(
		const size_t position, const size_t availableDigits) const;

private:
	SsdContent& _content;

	const CharSelectionStrategy& _centerSelectionStrategy;
	const CharSelectionStrategy& _leftSelectionStrategy;
	const CharSelectionStrategy& _rightSelectionStrategy;
	const CharSelectionStrategy* _currentPositioningStrategy;

	const CharSelectionStrategy& _beginningOverflowStrategy;
	const CharSelectionStrategy& _errorOverflowStrategy;
	const CharSelectionStrategy& _scrollOverflowStrategy;
	const CharSelectionStrategy* _currentOverflowStrategy;
};

#endif
