#ifndef CHARLIPLEXED_SSD_H
#define CHARLIPLEXED_SSD_H

#include "Counter.h"
#include "Pins.h"
#include "Timer.h"

#include "CharSelector.h"
#include "SevenSD.h"

#include "stddef.h"

class CharliplexedSSD : public SevenSD {
public:
	CharliplexedSSD(Pins<char>& segment_pins, Timer& timer,
		const unsigned long periodUs, Counter& counter,
		const CharSelector& selector);

	virtual ~CharliplexedSSD();

	virtual void updateDisplay();
	virtual size_t availableDigits() const;

private:
	Pins<char>& _segment_pins;
	Counter& _counter;
	const CharSelector& _selector;

	unsigned char _activeSegment;
};

#endif
