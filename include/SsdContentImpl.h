#ifndef SSD_CONTENT_IMPL_H
#define SSD_CONTENT_IMPL_H

#include "SegmentsDict.h"
#include "SsdContent.h"

#include <cstring>

#include "stddef.h"

template <size_t N>
class SsdContentImpl : public SsdContent {
public:
	SsdContentImpl(const SegmentsDict& segmentsDict) :
		_segmentsDict(segmentsDict)
	{
	// @todo assert
	}

	virtual ~SsdContentImpl() {}

	virtual void writeString(const char* const entry)
	{
		size_t entryLength = strlen(entry);

		size_t contentIndex = 0;
		size_t entryIndex = 0;

		for (size_t i = 0; i < N; ++i)
		{
			_content[i] = '\0';
		}
		for (size_t i = 0; i < N/8 + 1; ++i)
		{
			_dotsArray[i] = 0;
		}

		// ....Hi....
		while (contentIndex < N && entryIndex < entryLength)
		{
			if (entry[entryIndex] == '.')
			{
				if (contentIndex > 0)
				{
					size_t div = (contentIndex - 1) / 8;
					size_t mod = (contentIndex - 1) % 8;

					_dotsArray[div] = _dotsArray[div] | (1 << mod);
				}
				++entryIndex;
			}
			else
			{
				if (contentIndex < N - 1)
				{
					_content[contentIndex] = entry[entryIndex];
				}
				++contentIndex;
				++entryIndex;
			}
		}
	}

	virtual size_t length() const
	{
		return strlen(_content);
	}

	virtual unsigned char getSegments(const size_t position) const
	{
		if (position >= length() )
		{
			return _segmentsDict.getSegments(' ', false);
		}

		bool hasDot = (_dotsArray[position/8] & (1 << position % 8)) != 0;
		return _segmentsDict.getSegments(
			_content[position], hasDot);
	}

	virtual unsigned char getSegments(const char value, const bool withDot) const
	{
		return _segmentsDict.getSegments(value, withDot);
	}

private:
	char _content[N];
	const SegmentsDict& _segmentsDict;
	char _dotsArray[N/8 + 1];
};

#endif
