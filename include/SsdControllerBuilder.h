#ifndef SSD_CONTROLLER_BUILDER_H
#define SSD_CONTROLLER_BUILDER_H

#include "include/SubjectImpl.h"

#include "ArduinoPins.h"
#include "Counter.h"
#include "LoopEvent.h"
#include "PinsDefinition.h"
#include "TimerImpl.h"

#include "CharSelectionCenter.h"
#include "CharSelectionLeft.h"
#include "CharSelectionRight.h"
#include "CharSelectionError.h"
#include "CharSelectionScroll.h"
#include "CharSelector.h"
#include "SegmentsDict.h"
#include "SsdContentImpl.h"
#include "SsdControllerImpl.h"
#include "MultiplexedSSD.h"

template <
	class TSSD = MultiplexedSSD,
	size_t PERIOD_US = 2000,
	size_t SCROLL_MILLIS = 500,
	size_t EDGE_SCROLL_MILLIS = 1000,
	size_t NUM_SEGMENTS = 8,
	size_t NUM_DISPLAYS = 4,
	size_t MAX_CONTENT_LENGTH = 120
>
class SsdControllerBuilder
{
public:
	SsdControllerBuilder(
		PinsDefinition<NUM_SEGMENTS> segmentPinsId,
		SubjectImpl<LoopEvent>& loopSubject,
		Counter& counter) :
		_segment_pins_id(segmentPinsId),
		_segment_pins(_segment_pins_id.id, _segment_pins_id.activeHigh),
		_segmentsDict(),
		_centerSelectionStrategy(),
		_leftSelectionStrategy(),
		_rightSelectionStrategy(),
		_errorOverflowStrategy(),
		_scrollOverflowStrategy(SCROLL_MILLIS, EDGE_SCROLL_MILLIS, loopSubject),
		_content(_segmentsDict),
		_selector(_content, _centerSelectionStrategy, _leftSelectionStrategy,
			_rightSelectionStrategy, _leftSelectionStrategy,
			_errorOverflowStrategy, _scrollOverflowStrategy),
		_ssdTimer(loopSubject),
		_sevenSD(_segment_pins, _ssdTimer, PERIOD_US, counter, _selector),
		_controller(_sevenSD, _selector)
	{
	}

	virtual ~SsdControllerBuilder() {}

	virtual SsdController& get()
	{
		return _controller;
	}

private:
	PinsDefinition<NUM_SEGMENTS> _segment_pins_id;
	ArduinoPins<char, NUM_SEGMENTS> _segment_pins;
	SegmentsDict _segmentsDict;
	CharSelectionCenter _centerSelectionStrategy;
	CharSelectionLeft _leftSelectionStrategy;
	CharSelectionRight _rightSelectionStrategy;
	CharSelectionError _errorOverflowStrategy;
	CharSelectionScroll _scrollOverflowStrategy;
	SsdContentImpl<MAX_CONTENT_LENGTH> _content;
	CharSelector _selector;
	TimerImpl _ssdTimer;
	TSSD _sevenSD;
	SsdControllerImpl _controller;
};

#endif
